# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load in 

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import torch
from torch import optim
import torch.nn as nn
import csv
import matplotlib.pyplot as plt
# Input data files are available in the "../input/" directory.
# For example, running this (by clicking run or pressing Shift+Enter) will list the files in the input directory
import os
print(os.listdir("./input"))

# Any results you write to the current directory are saved as output.

class Model(nn.Module):
    def __init__(self, width, num_classes):
        super(Model, self).__init__()
        self.layer1 = nn.Conv2d(1, width//4, 3, stride=2)
        self.relu = nn.ReLU()
        self.layer2 = nn.Conv2d(width//4, width//2, 3, stride=2)
        self.layer3 = nn.Conv2d(width//2, width//2, 3, stride=2, padding=1)
        self.layer4 = nn.Conv2d(width//2, width, 3, stride=2, padding=1)
        self.layer5 = nn.Conv2d(width, 2*width, 12, stride=1)
        self.layer6 = nn.Conv2d(2*width, 2*width, 1)
        self.layer7 = nn.Conv2d(2*width, num_classes, 1)
        
    def forward(self, x):
        x = x.unsqueeze(1)
        out1 = self.layer1(x)
        input2 = self.relu(out1)
        out2 = self.layer2(input2)
        input3 = self.relu(out2)
        out3 = self.layer3(input3)
        input4 = self.relu(out3)
        out4 = self.layer4(input3)
        input5 = self.relu(out4)
        out5 = self.layer5(input5)
        input6 = self.relu(out5)
        out6 = self.layer6(input6)
        input7 = self.relu(out6)
        out7 = self.layer7(input7)
        return out7.squeeze()
    
    def compute_loss(self, x, target):
        logits = nn.functional.log_softmax(self.forward(x))
        loss = nn.functional.nll_loss(logits, target)
        return loss
    
    def predict(self, x):
        out = self.forward(x)
        _, label = out.max(dim=-1)
        return label


def train(train_data, train_label, valid_data, valid_label, model, optimizer, epoch, batch_size):
    model.train()
    start  = 0
    end = batch_size
    while end <= train_data.size()[0]:
        data = train_data[start:end]
        target = train_label[start:end]
        optimizer.zero_grad()
        loss = model.compute_loss(data, target)
        loss.backward()
        optimizer.step()
        print("Train epoch ", epoch, "loss :", loss.item())
        start = end
        end = end + batch_size

def test(test_data, model, batch_size):
    model.eval()
    start  = 0
    end = batch_size
    pred = []
    with torch.no_grad():
        while end <= test_data.size()[0]:
            data = test_data[start:end]
            pred_label = model.predict(data)
            pred = pred + list(pred_label)
            start = end
            end = end + batch_size

    return pred

def validation(valid_data, valid_label, model, batch_size):
    model.eval()
    valid_loss = 0
    correct = 0
    start  = 0
    end = batch_size
    pred = []
    n = valid_data.size()[0]
    with torch.no_grad():
        while end <= n:
            data = valid_data[start:end]
            target = valid_label[start:end]
            pred_label = model.predict(data)
            pred = pred + list(pred_label)
            valid_loss += model.compute_loss(data, target).item() # sum up batch loss
            correct += pred_label.eq(target.view_as(pred_label)).sum().item()
            start = end
            end = end + batch_size

    valid_loss /= n
    print('\nValidation set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        valid_loss, correct, n,
        100. * correct / n))


class_name_to_index = dict([
        ('sink', 0), ('pear', 1), ('moustache', 2), ('nose', 3), ('skateboard', 4), ('penguin', 5), ('peanut', 6), ('skull', 7), ('panda', 8),
        ('paintbrush', 9), ('nail', 10), ('apple', 11), ('rifle', 12), ('mug', 13), ('sailboat', 14), ('pineapple', 15), ('spoon', 16), ('rabbit', 17),
        ('shovel', 18), ('rollerskates', 19), ('screwdriver', 20), ('scorpion', 21), ('rhinoceros', 22), ('pool', 23), ('octagon', 24), ('pillow', 25),
        ('parrot', 26), ('squiggle', 27), ('mouth', 28), ('empty', 29), ('pencil', 30)
        ])

def dataLoader():
    DATA = np.load('./input/train_images.npy', encoding='latin1')
    LABELS = np.genfromtxt(
        './input/train_labels.csv',
        names=True,
        delimiter=',',
        dtype=[('Id', 'i8'), ('Category', 'S20')]
    )

    label = []
    x = []
    for i in range(len(LABELS)):
        class_name = LABELS[i][1].decode("utf-8")
        index = class_name_to_index[class_name]
        label.append(index)
        x.append(DATA[i][1])

    label =  np.array(label)
    
    X = np.stack(x)
    X = X.reshape(X.shape[0], 100, 100).astype('float32')

    n = int(0.8*X.shape[0])
    train_data = torch.from_numpy(X[:n])
    train_label = torch.from_numpy(label[:n])
    valid_data = torch.from_numpy(X[n:])
    valid_label = torch.from_numpy(label[n:])

    return train_data, train_label, valid_data, valid_label 

def testDataLoader():
    DATA = np.load('./input/test_images.npy', encoding='latin1')
    x = []
    for i in range(len(DATA)):
        x.append(DATA[i][1])

    X = np.stack(x)
    X = X.reshape(X.shape[0], 100, 100).astype('float32')

    test_data = torch.from_numpy(X)
    
    return test_data

train_data, train_label, valid_data, valid_label = dataLoader()
test_data = testDataLoader()

width, num_classes = 64, 31
epochs = 10
batch_size = 100
model = Model(width, num_classes)
optimizer = optim.Adam(model.parameters(), lr=0.001, betas=(0.9, 0.999), eps=1e-08)

for epoch in range(1, epochs + 1):
    train(train_data, train_label, valid_data, valid_label, model, optimizer, epoch, batch_size)

validation(valid_data, valid_label, model, batch_size)
prediction = test(test_data, model, batch_size)

pred_list = []
for i in range(len(prediction)):
    label_name = list(class_name_to_index.keys())[list(class_name_to_index.values()).index(prediction[i])]
    pred_list.append([i, label_name])

 
data_list = [["Id", "Category"]] + pred_list
 
myFile = open('submission.csv', 'w')
with myFile:
    writer = csv.writer(myFile)
    writer.writerows(data_list)
     





